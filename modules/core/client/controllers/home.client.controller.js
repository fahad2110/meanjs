(function () {
  'use strict';

  angular
    .module('core')
    .controller('HomeController', HomeController);
	HomeController.$inject = ['ArticlesService'];
  function HomeController(ArticlesService) {
    var vm = this;
    vm.articles = ArticlesService.query();
  }
}());
